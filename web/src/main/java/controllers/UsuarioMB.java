package controllers;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import dominio.Usuario;
import negocio.LoginService;

@ManagedBean
@RequestScoped
public class UsuarioMB {
	private Usuario usuario;
	
	@EJB
	private LoginService loginService;
	
	public UsuarioMB() {
		usuario = new Usuario();
	}
	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String login() {
		int res = loginService.login(usuario.getLogin(), usuario.getSenha());
		if (res == 1) {
			return "/interna/listaChamados.jsf";
		} else if (res == 0) {
			FacesMessage msg = new FacesMessage("Usu�rio ou senha incorreta.");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage("", msg);
			return null;
		} else {
			FacesMessage msg = new FacesMessage("Usuario n�o foi encontrado.");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage("", msg);
			return null;
		}
	}
}