package controllers;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import dominio.Chamado;
import negocio.ChamadosService;

@ManagedBean
@SessionScoped
public class ChamadoMB {
	private Chamado chamado;
	
	@EJB
	private ChamadosService chamadoService;
	
	private List<Chamado> listaChamados;
	
	
	public ChamadoMB() {
		chamado = new Chamado();
		listaChamados = new ArrayList<Chamado>(); 
	}
	
	public Chamado getChamado() {
		return chamado;
	}

	public void setChamado(Chamado chamado) {
		this.chamado = chamado;
	}
	
	public List<Chamado> getListaChamados() {
		setListaChamados(chamadoService.listarChamados());
		return listaChamados;
	}

	public void setListaChamados(List<Chamado> listaChamados) {
		this.listaChamados = listaChamados;
	}

	public String cadastrar() {
		chamadoService.cadastrarChamado(chamado);
		chamado = new Chamado();
		return "/interna/listaChamados.jsf";
	}
	
	public String novo() {
		return "/interna/cadastraChamado.jsf";
	}
}
