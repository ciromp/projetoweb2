package dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import dominio.Chamado;

@Stateless
public class ChamadoDAO {
	@PersistenceContext
	private EntityManager em;
	
	public void salvar(Chamado c) {
		em.persist(c);
	}
	
	public void atualizar(Chamado c) {
		em.merge(c);
	}
	
	public void remover(Chamado c) {
		c = em.find(Chamado.class, c.getId());
		em.remove(c);
	}
	
	@SuppressWarnings("unchecked")
	public List<Chamado> listar() {
		String qs = "select c from Chamado c";
		Query q = em.createQuery(qs);
		return (List<Chamado>) q.getResultList();
	}
	
	public Chamado buscarChamadoTitulo(String titulo) {
		String qs = "select c from Chamado c where c.titulo = :titulo";
		Query q = em.createQuery(qs);
		q.setParameter("titulo", titulo);
		try {
			return (Chamado) q.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
}