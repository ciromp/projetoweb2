package negocio;

import java.util.List;

import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import dao.ChamadoDAO;
import dominio.Chamado;

@Stateful
public class ChamadosService {
	
	@Inject
	private ChamadoDAO chamadoDAO;
	
	public List<Chamado> listarChamados() {
		return chamadoDAO.listar();
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void cadastrarChamado(Chamado chamado) {
		Chamado c = chamadoDAO.buscarChamadoTitulo(chamado.getTitulo());
		chamado.setStatus("Aguardando revis�o");
		if (c == null) {
			chamadoDAO.salvar(chamado);
		} else {
			chamadoDAO.atualizar(chamado);
		}
	}
}
