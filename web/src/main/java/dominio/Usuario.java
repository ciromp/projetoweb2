package dominio;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="usuarios")
public class Usuario {
	
	
	
	@Id
	@Column(nullable=false)
	private String login;
	
	@Column(nullable=false)
	private String senha;
	

	
	@ManyToMany(cascade=CascadeType.ALL)
	@JoinTable(name="usuario_chamado",
		joinColumns={@JoinColumn(name="usuario_id")},
		inverseJoinColumns={@JoinColumn(name="chamado_id")})
	private List<Chamado> chamados;
	
	
	public Usuario() {
		this.chamados = new ArrayList<Chamado>();
	}
	
	public Usuario(String login, String senha) {
		this.login = login;
		this.senha = senha;
		this.chamados = new ArrayList<Chamado>();
	}

	public String getLogin() {
		return login;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	
	public String getSenha() {
		return senha;
	}
	
	public void setSenha(String senha) {
		this.senha = senha;
	}

	public List<Chamado> getChamados() {
		return chamados;
	}

	public void setContatos(List<Chamado> chamados) {
		this.chamados = chamados;
	}
}