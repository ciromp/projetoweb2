package dominio;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="chamados")
public class Chamado {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	@Column(nullable=false)
	private String titulo;
	
	@Column(nullable=false)
	private String categoria;
	@Column(nullable=false)
	private String descricao;
	
	private String status;
	
	@ManyToMany(mappedBy="chamados")
	private List<Usuario> usuarios;
	
	
	public Chamado() {
		this.usuarios = new ArrayList<Usuario>();
	}
	
	public Chamado(String titulo, String categoria, String descricao, String status) {
		this.titulo = titulo;
		this.categoria = categoria;
		this.descricao = descricao;
		this.status = status;
		this.usuarios = new ArrayList<Usuario>();
		
	}

	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getDescricao() {
		return descricao;
	}
	

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}


}